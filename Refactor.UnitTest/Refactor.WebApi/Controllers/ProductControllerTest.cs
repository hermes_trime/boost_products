﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Refactor.Data.Repositories.Interfaces;
using refactor_this.Controllers;
using System.Web.Http.Results;
using refactor_this.Models;
using System.Collections.Generic;

namespace Refactor.UnitTest.Refactor.WebApi.Controllers
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public void GetProduct_Existing_ReturnsOk()
        {
            // Arrange
            var mockProductOptionRepository = new Mock<IProductOptionRepository>();
            var mockProductRepository = new Mock<IProductRepository>();

            mockProductRepository.Setup(x => x.Read(new Guid("de1287c0-4b15-4a7b-9d8a-dd21b3cafec3")))
                .Returns(new Data.Models.Product
                        {
                            Id = new Guid("de1287c0-4b15-4a7b-9d8a-dd21b3cafec3"),
                            Name = "Apple iPhone 6S",
                            Description = "Newest mobile product from Apple.",
                            Price = (decimal)1299.99, 
                            DeliveryPrice = (decimal)15.99
                        });

            var controller = new ProductsController(mockProductRepository.Object, mockProductOptionRepository.Object);

            // Act
            var actionResult = controller.GetProduct(new Guid("de1287c0-4b15-4a7b-9d8a-dd21b3cafec3"));
            var contentResult = actionResult as OkNegotiatedContentResult<Product>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual("Apple iPhone 6S", contentResult.Content.Name);
        }

        [TestMethod]
        public void GetProduct_NonExisting_ReturnsNotFound()
        {
            // Arrange
            var mockProductOptionRepository = new Mock<IProductOptionRepository>();
            var mockProductRepository = new Mock<IProductRepository>();

            Data.Models.Product nullResponse = null;
            mockProductRepository.Setup(x => x.Read(new Guid("00000000-0000-0000-0000-000000000000")))
                .Returns(nullResponse);

            var controller = new ProductsController(mockProductRepository.Object, mockProductOptionRepository.Object);

            // Act
            var actionResult = controller.GetProduct(new Guid("00000000-0000-0000-0000-000000000000"));

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void GetProductsByName_Existing_ReturnsOk()
        {
            // Arrange
            var mockProductOptionRepository = new Mock<IProductOptionRepository>();
            var mockProductRepository = new Mock<IProductRepository>();

            mockProductRepository.Setup(x => x.Read("Apple"))
                .Returns(new List<Data.Models.Product>
                { new Data.Models.Product{
                    Id = new Guid("de1287c0-4b15-4a7b-9d8a-dd21b3cafec3"),
                    Name = "Apple iPhone 6S",
                    Description = "Newest mobile product from Apple.",
                    Price = (decimal)1299.99,
                    DeliveryPrice = (decimal)15.99
                    }
                });

            var controller = new ProductsController(mockProductRepository.Object, mockProductOptionRepository.Object);

            // Act
            var actionResult = controller.GetProductsByName("Apple");
 
            // Assert
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(actionResult.Items.Count, 1);
            Assert.AreEqual("Apple iPhone 6S", actionResult.Items[0].Name);
        }
    }
}
