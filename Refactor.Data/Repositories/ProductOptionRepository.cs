﻿using Refactor.Data.Repositories.Interfaces;
using Refactor.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Refactor.Data.Repositories
{
    public class ProductOptionRepository : Repository, IProductOptionRepository
    {
        public void Create(ProductOption productOption)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"insert into productoption (id, productid, name, description) values ('{Guid.NewGuid()}', '{productOption.ProductId}', '{productOption.Name}', '{productOption.Description}')", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(Guid id)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"delete from productoption where id = '{id}'", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void DeleteAll(Guid productId)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"delete from productoption where productid = '{productId}'", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public IEnumerable<ProductOption> ReadAll(Guid productId)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"select id, productid, name, description from productoption where productid = '{productId}'", connection))
                {
                    var Items = new List<ProductOption>();
                    var rdr = command.ExecuteReader();

                    while (rdr.Read())
                    {
                        Items.Add(MapToProductOption(rdr));
                    }
                    return Items;
                }
            }
        }

        public ProductOption Read(Guid productId, Guid id)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"select id, productid, name, description from productoption where id = '{id}' and productid = '{productId}'", connection))
                {
                    var rdr = command.ExecuteReader();
                    if (!rdr.Read())
                        return null;

                    return MapToProductOption(rdr);
                }
            }
        }

        public void Update(ProductOption productOption)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"update productoption set name = '{productOption.Name}', description = '{productOption.Description}' where id = '{productOption.Id}'", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private ProductOption MapToProductOption(SqlDataReader rdr)
        {
            return new ProductOption
            {
                Id = Guid.Parse(rdr["Id"].ToString()),
                ProductId = Guid.Parse(rdr["ProductId"].ToString()),
                Name = rdr["Name"].ToString(),
                Description = (DBNull.Value == rdr["Description"]) ? null : rdr["Description"].ToString()
            };
        }

    }
}
