﻿using Refactor.Data.Repositories.Interfaces;
using Refactor.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Refactor.Data.Repositories
{
    public class ProductRepository : Repository, IProductRepository
    {
        public void Create(Product product)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"insert into product (id, name, description, price, deliveryprice) values ('{Guid.NewGuid()}', '{product.Name}', '{product.Description}', {product.Price}, {product.DeliveryPrice})", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(Product product)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"update product set name = '{product.Name}', description = '{product.Description}', price = {product.Price}, deliveryprice = {product.DeliveryPrice} where id = '{product.Id}'", connection))
                {
                    command.ExecuteNonQuery();
                }
            }

        }
        
        public Product Read(Guid id)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"select Id, Name, Description, Price, DeliveryPrice from product where id = '{id}'", connection))
                {
                    var rdr = command.ExecuteReader();
                    if (!rdr.Read())
                        return null;

                    return MapToProduct(rdr);
                }
            }
        }

        public IEnumerable<Product> Read(string name = "")
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand(
                    $@"select Id, Name, Description, Price, DeliveryPrice from product 
                    where '{name?.Trim()}' = '' 
                        or ('{name?.Trim()}' <> '' and lower(name) like '%{name?.ToLower()}%')"
                    , connection))
                {
                    var Items = new List<Product>();
                    var rdr = command.ExecuteReader();

                    while (rdr.Read())
                    {
                        Items.Add(MapToProduct(rdr));
                    }
                    return Items;
                }
            }
        }

        public void Delete(Guid id)
        {

            var productOptionRepo = new ProductOptionRepository();
            productOptionRepo.DeleteAll(id);

            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand($"delete from product where id = '{id}'", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        private Product MapToProduct(SqlDataReader rdr)
        {
            return new Product
            {
                Id = Guid.Parse(rdr["Id"].ToString()),
                Name = rdr["Name"].ToString(),
                Description = (DBNull.Value == rdr["Description"]) ? null : rdr["Description"].ToString(),
                Price = decimal.Parse(rdr["Price"].ToString()),
                DeliveryPrice = decimal.Parse(rdr["DeliveryPrice"].ToString())
            };
        }
      
    }
}
