﻿using Refactor.Data.Models;
using System.Collections.Generic;
using System;

namespace Refactor.Data.Repositories.Interfaces
{
    public interface IProductRepository
    {
        void Create(Product product);
        void Update(Product product);
        void Delete(Guid id);
        IEnumerable<Product> Read(string name = "");
        Product Read(Guid id);
    }
}
