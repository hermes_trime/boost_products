﻿using System.Collections.Generic;
using Refactor.Data.Models;
using System;

namespace Refactor.Data.Repositories.Interfaces
{
    public interface IProductOptionRepository
    {
        void Create(ProductOption productOption);
        void Update(ProductOption productOption);
        void Delete(Guid id);
        void DeleteAll(Guid productId);
        IEnumerable<ProductOption> ReadAll(Guid productId);
        ProductOption Read(Guid productId,Guid id);
    }
}
