﻿using System.Data.SqlClient;
using System.Configuration;
using System;

namespace Refactor.Data
{
    public abstract class Repository
    {
        private readonly string connStr;

        public Repository()
        {
            connStr = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            connStr = connStr.Replace("|DataDirectory|", System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data"));
        }

        public SqlConnection GetConnection()
        {
            var connection = new SqlConnection(connStr);
            connection.Open();
            return connection;
        }
    }
}
