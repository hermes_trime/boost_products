﻿using System;
using System.Net;
using System.Web.Http;
using refactor_this.Models;
using refactor_this.Helpers;
using Refactor.Data.Repositories.Interfaces;

namespace refactor_this.Controllers
{
    //[RoutePrefix("products")]
    public class ProductsController : ApiController
    {
        IProductRepository productRepository;
        IProductOptionRepository productOptionRepository;

        public ProductsController(IProductRepository productRepo, IProductOptionRepository productOptionRepo)
        {
            productRepository = productRepo;
            productOptionRepository = productOptionRepo;
        }

        /// <summary>
        /// GET /products
        /// Gets all products.
        /// </summary>
        /// <returns>Products</returns>
        //[Route]
        [HttpGet]
        public Products GetProducts()
        {
            var products = productRepository.Read();
            return ProductMapper.MapFromData(products);
        }

        /// <summary>
        /// GET /products?name={name}
        /// Finds all products matching the specified name
        /// </summary>
        /// <param name="name">Product name</param>
        /// <returns>Products</returns>
        [Route]
        [HttpGet]
        public Products GetProductsByName(string name)
        {
            var products = productRepository.Read(name);
            return ProductMapper.MapFromData(products);
        }

        /// <summary>
        /// GET /products/{id}
        /// Gets the product that matches the specified ID.
        /// </summary>
        /// <param name="id">Product Id. Id is a GUID</param>
        /// <returns>Product</returns>
        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult GetProduct(Guid id)
        {
            var product = productRepository.Read(id);

            if (product == null)
                return NotFound();

            return Ok(ProductMapper.MapFromData(product));
        }

        /// <summary>
        /// POST /products
        /// Creates a new product.
        /// </summary>
        /// <param name="product">Product data</param>
        [Route]
        [HttpPost]
        public void CreateProduct(Product product)
        {
            productRepository.Create(ProductMapper.MapToData(product));
        }

        /// <summary>
        /// PUT /products/{id}
        /// Updates a product,
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="product">Product data</param>
        [Route("{id}")]
        [HttpPut]
        public void UpdateProduct(Guid id, Product product)
        {
            product.Id = id;
            productRepository.Update(ProductMapper.MapToData(product));
        }

        /// <summary>
        /// DELETE /products/{id}
        /// Deletes a product and its options
        /// </summary>
        /// <param name="id">Product Id</param>
        [Route("{id}")]
        [HttpDelete]
        public void DeleteProduct(Guid id)
        {
            productRepository.Delete(id);
        }


        /// <summary>
        /// GET /products/{id}/options
        /// Finds all options for a specified product.
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>ProductOptions</returns>
        [Route("{productId}/options")]
        [HttpGet]
        public ProductOptions GetOptions(Guid productId)
        {
            var options = productOptionRepository.ReadAll(productId);
            return ProductOptionMapper.MapFromData(options);
        }

        /// <summary>
        /// GET /products/{id}/options/{optionId}
        /// Finds the specified product option for the specified product.
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="id">Option Id</param>
        /// <returns>ProductOption</returns>
        [Route("{productId}/options/{id}")]
        [HttpGet]
        public IHttpActionResult GetOption(Guid productId, Guid id)
        {
            var option = productOptionRepository.Read(productId, id);

            if (option == null)
                return NotFound();

            return Ok(ProductOptionMapper.MapFromData(option));
        }

        /// <summary>
        /// POST /products/{id}/options
        /// Adds a new product option to the specified product.
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="option">Option data</param>
        [Route("{productId}/options")]
        [HttpPost]
        public void CreateOption(Guid productId, ProductOption option)
        {
            option.ProductId = productId;
            productOptionRepository.Create(ProductOptionMapper.MapToData(option));
        }

        /// <summary>
        /// PUT /products/{id}/options/{optionId}
        /// Updates the specified product option.
        /// </summary>
        /// <param name="id">Option Id</param>
        /// <param name="option">Option data</param>
        [Route("{productId}/options/{id}")]
        [HttpPut]
        public void UpdateOption(Guid id, ProductOption option)
        {
            option.Id = id;
            productOptionRepository.Update(ProductOptionMapper.MapToData(option));
        }

        /// <summary>
        /// DELETE /products/{id}/options/{optionId}
        /// Deletes the specified product option.
        /// </summary>
        /// <param name="id">Option Id</param>
        [Route("{productId}/options/{id}")]
        [HttpDelete]
        public void DeleteOption(Guid id)
        {
            productOptionRepository.Delete(id);
        }
    }
}
