﻿using refactor_this.Models;
using System.Collections.Generic;
using Data = Refactor.Data.Models;

namespace refactor_this.Helpers
{
    public static class ProductOptionMapper
    {
        public static ProductOption MapFromData(Data.ProductOption productOption)
        {
            return new ProductOption(
                        productOption.Id,
                        productOption.ProductId,
                        productOption.Name,
                        productOption.Description
                    );
        }

        public static Data.ProductOption MapToData(ProductOption productOption)
        {
            return new Data.ProductOption
            { 
                Id = productOption.Id,
                ProductId = productOption.ProductId,
                Name = productOption.Name,
                Description = productOption.Description
            };
        }

        public static ProductOptions MapFromData(IEnumerable<Data.ProductOption> productOptions)
        {
            var result = new ProductOptions();

            foreach (var productOption in productOptions)
            {
                result.AddItem(MapFromData(productOption));
            }

            return result;
        }
    }
}