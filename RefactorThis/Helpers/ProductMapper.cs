﻿using refactor_this.Models;
using System.Collections.Generic;
using Data = Refactor.Data.Models;

namespace refactor_this.Helpers
{
    public static class ProductMapper
    {
        public static Product MapFromData(Data.Product product)
        {
            return new Product(
                        product.Id,
                        product.Name,
                        product.Description,
                        product.Price,
                        product.DeliveryPrice
                    );
        }

        public static Data.Product MapToData(Product product)
        {
            return new Data.Product
            { 
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                DeliveryPrice = product.DeliveryPrice
            };
        }

        public static Products MapFromData(IEnumerable<Data.Product> products)
        {
            var result = new Products();

            foreach (var product in products)
            {
                result.AddItem(MapFromData(product));
            }

            return result;
        }
    }
}