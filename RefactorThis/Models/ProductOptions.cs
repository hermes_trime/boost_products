﻿using System.Collections.Generic;

namespace refactor_this.Models
{
    public class ProductOptions
    {
        public IList<ProductOption> Items { get; private set; }

        public ProductOptions()
        {
            Items = new List<ProductOption>();
        }

        public void AddItem(ProductOption productOption)
        {
            Items.Add(productOption);
        }
    }
}