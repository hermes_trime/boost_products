﻿using System.Collections.Generic;

namespace refactor_this.Models
{
    public class Products
    {
       public IList<Product> Items { get; private set; }

        public Products()
        {
            Items = new List<Product>();
        }

        public void AddItem(Product product)
        {
            Items.Add(product);
        }
      
    }
}